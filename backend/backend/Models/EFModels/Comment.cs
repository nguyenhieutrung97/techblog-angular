﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.EFModels
{
    public class Comment
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(200)]
        public string CommentContent { get; set; }
        [Required]
        public DateTime CreatedTime { get; set; }

        public Guid? UserId { get; set; }
        public User User { get; set; }
        public Guid BlogId { get; set; }
        public Blog Blog { get; set; }

    }
}
