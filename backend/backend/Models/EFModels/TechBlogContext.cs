﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.EFModels
{
    public class TechBlogContext : DbContext
    {
        public TechBlogContext(DbContextOptions<TechBlogContext> options)
            : base(options)
        {
        }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<BlogTag> BlogTags { get; set; }
        public DbSet<BlogDetail> BlogDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlogTag>().HasKey(bt => new { bt.BlogId, bt.TagId });

            modelBuilder.Entity<User>().Property(u => u.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Tag>().Property(t => t.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Blog>().Property(b => b.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<BlogDetail>().Property(bd => bd.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Comment>().Property(c => c.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Role>().Property(r => r.Id).HasDefaultValueSql("NEWID()");

            modelBuilder.Entity<User>().HasIndex(u => u.Email);
            modelBuilder.Entity<Tag>().HasIndex(t => t.Name);
            modelBuilder.Entity<Blog>().HasIndex(b => b.Title);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.;Database=TechBlogDB;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(@"Server=HSSSC1PCL01492\TRUNG;Database=TechBlogDB;Trusted_Connection=True;");
        }
    }
}
