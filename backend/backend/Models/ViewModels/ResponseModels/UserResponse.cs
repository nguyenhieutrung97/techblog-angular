﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels.ResponseModels
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string NameDisplay { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreatedTime { get; set; }
        public int BlogSum { get; set; }

        public string Role { get; set; }
        
    }
}
