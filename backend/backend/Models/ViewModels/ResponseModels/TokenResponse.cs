﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels.ResponseModels
{
    public class TokenResponse
    {
        public string NameDisplay { get; set; }
        public string Token { get; set; }

        public string Role { get; set; }
    }
}
