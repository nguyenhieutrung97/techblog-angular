﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels
{
    public class TagResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
