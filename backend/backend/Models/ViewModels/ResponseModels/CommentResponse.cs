﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels.ResponseModels
{
    public class CommentResponse
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(200)]
        public string CommentContent { get; set; }
        [Required]
        public DateTime CreatedTime { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public string NameDisplayUser { get; set; }
        [Required]
        public string ImageUrlUser { get; set; }
        [Required]
        public Guid BlogId { get; set; }
        
    }
}
