﻿using backend.Models.EFModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels
{
    public class BlogResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedTime { get; set; }
        public string ImageUrl { get; set; }
        public string NameAuthor { get; set; }
        public string ImageAuthorUrl { get; set; }
        public List<TagResponse> Tags { get; set; }
    }
}
