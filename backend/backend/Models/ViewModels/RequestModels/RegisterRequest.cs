﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels
{
    public class RegisterRequest
    {
        public string Password { get; set; }
        public string NameDisplay { get; set; }
        public string Email { get; set; }
    }
}
