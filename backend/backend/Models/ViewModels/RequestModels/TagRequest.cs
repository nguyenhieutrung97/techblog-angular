﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels.RequestModels
{
    public class TagRequest
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
