﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels.RequestModels
{
    public class CommentRequest
    {
        public Guid BlogId { get; set; }
        public  string CommentContent { get; set; }
    }
}
