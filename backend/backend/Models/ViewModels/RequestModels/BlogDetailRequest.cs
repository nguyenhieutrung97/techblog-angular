﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.ViewModels
{
    public class BlogDetailRequest
    {
        [Required]
        public string Title { get; set; }
        [Required]
        [MinLength(20)]
        public string HtmlContent { get; set; }
        [Required]
        public List<TagResponse> Tags { get; set; } 
    }
}
