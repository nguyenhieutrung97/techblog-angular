﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Extensions;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Models.ViewModels.ResponseModels;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace backend.Repositories
{
    public enum RegisterState
    {
        Success, EmailExist, Failled
    }
    public interface IUserRepository : IModelRepository<UserResponse>
    {
        Task<TokenResponse> Login(LoginRequest login);
        Task<RegisterState> Register(RegisterRequest registerRequest);
        Task<UserResponse> GetByEmail(string email);
    }
    public class UserRepository : IUserRepository
    {
        private readonly TechBlogContext _context;
        private readonly Guid userRoleId = new Guid("daa3481e-e333-4add-aa3e-df35af99ef87");

        private readonly IConfiguration Configuration;
        public UserRepository(TechBlogContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public Task<List<UserResponse>> GetAll()
        {
            throw new NotImplementedException();
        }

        public async Task<UserResponse> GetById(Guid id)
        {
            User user = await _context.Users.Where(u => u.Id == id).Include(u => u.Role).Include(u => u.Blogs).FirstOrDefaultAsync().ConfigureAwait(false);
            if (user != null)
            {
                return new UserResponse { Id = id, NameDisplay = user.NameDisplay, Email = user.Email, ImageUrl = user.ImageUrl, CreatedTime = user.CreatedTime, BlogSum = user.Blogs.Count, Role = user.Role.Name };
            }
            else
            {
                return null;
            };
        }

        public async Task<UserResponse> GetByEmail(string email)
        {
            User user = await _context.Users.Where(u => u.Email == email).Include(u => u.Role).Include(u => u.Blogs).FirstOrDefaultAsync().ConfigureAwait(false);
            if (user != null)
            {
                return new UserResponse { Id = user.Id, NameDisplay = user.NameDisplay, Email = user.Email, ImageUrl = user.ImageUrl, CreatedTime = user.CreatedTime, BlogSum = user.Blogs.Count, Role = user.Role.Name };
            }
            else
            {
                return null;
            };
        }

        public async Task<TokenResponse> Login(LoginRequest login)
        {
            if (login != null)
            {
                string passwordHashed = Encryption.ComputeSha256Hash(login.Password);
                User user = await _context.Users.Where(user => user.Email == login.Email && user.Password == passwordHashed).Include(u => u.Role).FirstOrDefaultAsync().ConfigureAwait(false);
                if (user != default)
                {
                    var key = Configuration["JWTStorage:Secret"];
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var keyEncoded = Encoding.ASCII.GetBytes(key);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.NameIdentifier, user.Email),
                    new Claim(ClaimTypes.Role,user.Role.Name)
                    }),
                        Expires = DateTime.UtcNow.AddDays(3),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyEncoded), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var tokenResponse = new TokenResponse { NameDisplay = user.NameDisplay, Role = user.Role.Name, Token = tokenHandler.WriteToken(token) };
                    return tokenResponse;
                }
            }
            return null;
        }


        public async Task<RegisterState> Register(RegisterRequest registerRequest)
        {
            if (registerRequest != null)
            {
                if (await _context.Users.AnyAsync(u => u.Email == registerRequest.Email).ConfigureAwait(false) == true)
                {
                    return RegisterState.EmailExist;
                }
                string passordHashed = Encryption.ComputeSha256Hash(registerRequest.Password);
                User user = new User { Email = registerRequest.Email, NameDisplay = registerRequest.NameDisplay, Password = passordHashed, ImageUrl = "/images/users/user.jpg", RoleId = userRoleId, CreatedTime = System.DateTime.Now };
                try
                {
                    await _context.Users.AddAsync(user);
                    await _context.SaveChangesAsync().ConfigureAwait(false);
                    return RegisterState.Success;
                }
                catch (Exception)
                {
                    return RegisterState.Failled;
                }
            }
            return RegisterState.Failled;
        }
    }
}
