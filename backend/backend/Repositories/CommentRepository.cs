﻿using backend.Models.EFModels;
using backend.Models.ViewModels.RequestModels;
using backend.Models.ViewModels.ResponseModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Repositories
{
    public interface ICommentRepository:IModelRepository<CommentResponse>
    {
        Task<List<CommentResponse>> GetByBlogId(Guid blogId);
        Task<CommentResponse> Create(CommentRequest commentRequest, Guid userId, string imageUrlUser, string nameDisplayUser);
    }
    public class CommentRepository : ICommentRepository
    {
        private readonly TechBlogContext _context;
        public CommentRepository(TechBlogContext context)
        {
            _context = context;
        }
        public async Task<List<CommentResponse>> GetByBlogId(Guid blogId)
        {
            return await _context.Comments.Where(c => c.BlogId == blogId).Include(c => c.User)
                .OrderByDescending(c=>c.CreatedTime)
                .Select(c => new CommentResponse { Id=c.Id,BlogId = blogId, CommentContent = c.CommentContent, CreatedTime = c.CreatedTime, ImageUrlUser = c.User.ImageUrl, NameDisplayUser = c.User.NameDisplay, UserId = c.User.Id }).ToListAsync().ConfigureAwait(false);
        } 
        public async Task<CommentResponse> Create(CommentRequest commentRequest, Guid userId,string imageUrlUser,string nameDisplayUser)
        {
            Comment comment = new Comment { BlogId = commentRequest.BlogId, CommentContent = commentRequest.CommentContent, UserId = userId, CreatedTime = System.DateTime.Now };
            try
            {
                await _context.Comments.AddAsync(comment);
                await _context.SaveChangesAsync().ConfigureAwait(false);
                return new CommentResponse { BlogId=comment.BlogId,CommentContent=comment.CommentContent,CreatedTime=System.DateTime.Now,UserId=userId,ImageUrlUser=imageUrlUser,NameDisplayUser=nameDisplayUser};
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Task<List<CommentResponse>> GetAll()
        {
            throw new NotImplementedException();
        }

        public async Task<CommentResponse> GetById(Guid id)
        {
            CommentResponse comment = await _context.Comments.Where(c=>c.Id==id).Include(c=>c.User).Select(c=>new CommentResponse {Id=c.Id, BlogId=c.BlogId,CommentContent=c.CommentContent,CreatedTime=c.CreatedTime,ImageUrlUser=c.User.ImageUrl,NameDisplayUser=c.User.NameDisplay,UserId=c.User.Id}).FirstOrDefaultAsync().ConfigureAwait(false);
            if(comment != null)
            {
                return comment;
            }
            return null;
        }
    }
}
