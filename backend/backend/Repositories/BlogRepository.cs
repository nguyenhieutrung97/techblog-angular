﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Extensions;
using System.Web;
using System.Globalization;
using Microsoft.AspNetCore.Http;

namespace backend.Repositories
{

    public interface IBlogRepository : IModelRepository<BlogResponse>
    {
        Task<BlogDetailResponse> GetDetailById(Guid id);
        Task<List<BlogResponse>> GetBySearchString(string searchString);
        Task<List<BlogResponse>> GetByConditionParameters(string tagName, int pageNumber, int pageSize);
        Task<BlogDetailResponse> Create(Guid userId, BlogDetailRequest blogDetailVM);
        Task<string> CreateDisplayImage(Guid blogId, IFormFile image);
        Task<string> CreateBlogImage(string emailUser, string folderName, IFormFile image);
        Task<bool> DeleteBlogImage(string emailUser, string folderName);
    }
    public class BlogRepository : IBlogRepository
    {
        private readonly TechBlogContext _context;
        private readonly IWebHostEnvironment _env;
        public BlogRepository(TechBlogContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }
        public async Task<List<BlogResponse>> GetAll()
        {
            List<Blog> blogs = await _context.Blogs.OrderByDescending(b => b.CreatedTime).ToListAsync().ConfigureAwait(false);
            List<Blog> blogResults = blogs;
            List<BlogResponse> blogResponses = new List<BlogResponse>();

            foreach (var blog in blogResults)
            {
                User user = _context.Users.Find(blog.UserId);
                List<TagResponse> tags = await _context.BlogTags.Where(blogTag => blogTag.BlogId == blog.Id).
                    Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => new TagResponse { Id = tag.Id, Name = tag.Name }).ToListAsync().ConfigureAwait(false);
                string imageThumbnail = blog.ImageUrl.Replace(".jpg", "-thumbnail.jpg", StringComparison.Ordinal);
                BlogResponse blogResponse = new BlogResponse
                {
                    Id = blog.Id,
                    Title = blog.Title,
                    ImageUrl = imageThumbnail,
                    CreatedTime = blog.CreatedTime,
                    ImageAuthorUrl = user.ImageUrl,
                    NameAuthor = user.NameDisplay,
                    Tags = tags
                };
                blogResponses.Add(blogResponse);
            }
            return blogResponses;
        }

        public Task<BlogResponse> GetById(Guid Id)
        {
            throw new NotImplementedException();
        }
        public async Task<BlogDetailResponse> GetDetailById(Guid id)
        {
            Blog blog = await _context.Blogs.FindAsync(id);
            if (blog == null)
            {
                return null;
            }
            Blog blogResult = blog;
            BlogDetail blogDetail = await _context.BlogDetails.Where(blogDetail => blogDetail.BlogId == id).FirstOrDefaultAsync().ConfigureAwait(false);
            BlogDetail blogDetailResult = blogDetail;
            User user = await _context.Users.Where(user => user.Id == blogResult.UserId).FirstOrDefaultAsync().ConfigureAwait(false);
            User userIdResult = user;

            if (blogDetailResult != null)
            {
                List<Tag> tags = await _context.BlogTags.Where(blogTag => blogTag.BlogId == blogResult.Id).
                    Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => tag).ToListAsync().ConfigureAwait(false);
                List<Tag> tagResults = tags;

                BlogDetailResponse blogDetailResponse = new BlogDetailResponse
                {
                    BlogId = blogResult.Id,
                    Title = blogResult.Title,
                    ImageUrl = blogResult.ImageUrl,
                    Tags = tagResults.Select(t => new TagResponse { Id = t.Id, Name = t.Name }).ToList(),
                    HtmlContent = blogDetailResult.HtmlContent,
                    CreatedTime = blogResult.CreatedTime,
                    NameAuthor = user.NameDisplay,
                    EmailAuthor = user.Email,
                    ImageAuthorUrl = userIdResult.ImageUrl
                };
                return blogDetailResponse;
            }
            else
            {
                return null;
            }
        }
        public async Task<List<BlogResponse>> GetByConditionParameters(string tagName, int pageNumber, int pageSize)
        {
            List<Blog> blogs;
            if (tagName != null)
            {
                blogs = await _context.BlogTags.Include(bt => bt.Tag).
                     Where(bt => bt.Tag.Name == tagName).Include(bt => bt.Blog)
                    .OrderByDescending(bt => bt.Blog.CreatedTime)
                    .Select(bt => new Blog { Id = bt.BlogId, Title = bt.Blog.Title, ImageUrl = bt.Blog.ImageUrl, CreatedTime = bt.Blog.CreatedTime, UserId = bt.Blog.UserId, User = bt.Blog.User })
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync().ConfigureAwait(false);
            }
            else
            {
                blogs = await _context.Blogs.OrderByDescending(b => b.CreatedTime)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .Include(b => b.User)
                    .ToListAsync().ConfigureAwait(false);
            }
            List<Blog> blogResults = blogs;
            List<BlogResponse> blogResponses = new List<BlogResponse>();
            //Generate Blogs Response object
            foreach (var blog in blogResults)
            {
                List<TagResponse> tags = await
                    _context.BlogTags.Where(blogTag => blogTag.BlogId == blog.Id).Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => new TagResponse { Id = tag.Id, Name = tag.Name }).ToListAsync().ConfigureAwait(false);
                string imageThumbnail = blog.ImageUrl.Replace(".jpg", "-thumbnail.jpg", StringComparison.Ordinal);
                BlogResponse blogResponse = new BlogResponse { Id = blog.Id, Title = blog.Title, ImageUrl = imageThumbnail, CreatedTime = blog.CreatedTime, ImageAuthorUrl = blog.User.ImageUrl, NameAuthor = blog.User.NameDisplay, Tags = tags };
                blogResponses.Add(blogResponse);
            }
            return blogResponses;
        }
        public async Task<string> CreateBlogImage(string emailUser, string folderName, IFormFile image)
        {
            try
            {
                string imageUrl = await FileService.UploadSingleImageOfBlog(_env.WebRootPath, image, emailUser, folderName, "description").ConfigureAwait(false);
                string pathImageRoot = _env.WebRootPath + imageUrl.Replace("/", "\\", StringComparison.Ordinal);
                FileService.ResizeImage(pathImageRoot, pathImageRoot, 600, 0);
                return imageUrl;

            }
            catch (Exception)
            {
                return null;
            }

        }
        public Task<bool> DeleteBlogImage(string emailUser, string folderName)
        {
            string pathFolderImage = $"{ _env.WebRootPath}\\images\\blogs\\ckeditor5\\{emailUser}\\{folderName}";
            return Task.Run(() => FileService.CheckAndDeleteDirectory(pathFolderImage));
        }
        public async Task<string> CreateDisplayImage(Guid blogId, IFormFile image)
        {
            try
            {
                string imageUrl = await FileService.UploadSingleImage(_env.WebRootPath, image, "blogs", blogId.ToString(), "blog_display").ConfigureAwait(false);
                string pathImageRoot = _env.WebRootPath + imageUrl.Replace("/", "\\", StringComparison.Ordinal);
                FileService.ResizeImage(pathImageRoot, pathImageRoot, 825, 0);
                FileService.ResizeImage(pathImageRoot, pathImageRoot.Replace(".jpg", "-thumbnail.jpg", StringComparison.Ordinal), 400, 160);
                return imageUrl;

            }
            catch (Exception)
            {
                return null;
            }

        }
        public async Task<BlogDetailResponse> Create(Guid userId, BlogDetailRequest blogDetailVM)
        {

            if (blogDetailVM != null)
            {
                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        Blog blog = new Blog { Title = blogDetailVM.Title, UserId = userId, ImageUrl = "", CreatedTime = System.DateTime.Now };
                        await _context.Blogs.AddAsync(blog);
                        await _context.SaveChangesAsync().ConfigureAwait(false);
                        blog.ImageUrl = "/images/blogs/" + blog.Id.ToString() + "/blog_display.jpg";
                        BlogDetail blogDetail = new BlogDetail { BlogId = blog.Id, HtmlContent = blogDetailVM.HtmlContent };
                        await _context.BlogDetails.AddAsync(blogDetail);
                        await _context.SaveChangesAsync().ConfigureAwait(false);


                        foreach (TagResponse tag in blogDetailVM.Tags)
                        {
                            await _context.BlogTags.AddAsync(new BlogTag { BlogId = blog.Id, TagId = tag.Id });
                        }
                        await _context.SaveChangesAsync().ConfigureAwait(false);
                        BlogDetailResponse blogDetailResponse = new BlogDetailResponse
                        {
                            BlogId = blog.Id,
                            Title = blog.Title,
                            ImageUrl = blog.ImageUrl,
                            Tags = blogDetailVM.Tags,
                            HtmlContent = blogDetail.HtmlContent,
                            CreatedTime = blog.CreatedTime
                        };
                        // Commit transaction if all commands succeed, transaction will auto-rollback
                        // when disposed if either commands fails
                        transaction.Commit();
                        return blogDetailResponse;
                    }
                    catch (Exception)
                    {
                        return null;
                    }

                }
            }
            return null;
        }
        public async Task<List<BlogResponse>> GetBySearchString(string searchString)
        {
            var blogResults = await _context.Blogs.Where(b => b.Title.Contains(searchString))
                .Include(b => b.User)
                .ToListAsync()
                .ConfigureAwait(false);
            List<BlogResponse> blogResponses = new List<BlogResponse>();
            //Generate Blogs Response object
            foreach (var blog in blogResults)
            {
                List<TagResponse> tags = await
                    _context.BlogTags.Where(blogTag => blogTag.BlogId == blog.Id).Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => new TagResponse { Id = tag.Id, Name = tag.Name }).ToListAsync().ConfigureAwait(false);
                string imageThumbnail = blog.ImageUrl.Replace(".jpg", "-thumbnail.jpg", StringComparison.Ordinal);
                BlogResponse blogResponse = new BlogResponse { Id = blog.Id, Title = blog.Title, ImageUrl = imageThumbnail, CreatedTime = blog.CreatedTime, ImageAuthorUrl = blog.User.ImageUrl, NameAuthor = blog.User.NameDisplay, Tags = tags };
                blogResponses.Add(blogResponse);
            }
            return blogResponses;
        }


    }
}
