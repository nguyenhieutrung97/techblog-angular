﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Models.ViewModels.RequestModels;

namespace backend.Repositories
{
    public interface ITagRepository : IModelRepository<TagResponse>
    {
        Task<TagResponse> Delete(Guid id);
        Task<TagResponse> Create(TagRequest tag);
        Task<TagResponse> Update(Guid id, TagRequest tagRequest);
        Task<bool> IsNameExisted(string name);
        Task<bool> IsIdExisted(Guid id);
        Task<int> SumBlogs(Guid tagId);
    }
    public class TagRepository : ITagRepository
    {
        private readonly TechBlogContext _context;
        public TagRepository(TechBlogContext context)
        {
            _context = context;
        }
        public async Task<List<TagResponse>> GetAll()
        {
            return await _context.Tags.Select(t => new TagResponse { Id = t.Id, Name = t.Name }).ToListAsync().ConfigureAwait(false);
        }

        public async Task<TagResponse> GetById(Guid id)
        {
            return await _context.Tags.Where(t => t.Id == id).Select(t => new TagResponse { Id = t.Id, Name = t.Name }).FirstOrDefaultAsync().ConfigureAwait(false);
        }
        public async Task<TagResponse> Delete(Guid id)
        {
            var tag = await _context.Tags.FindAsync(id);
            if (tag == null)
            {
                return null;
            }
            _context.Tags.Remove(tag);
            int affected = await _context.SaveChangesAsync();
            if (affected == 1)
            {
                return new TagResponse { Id = tag.Id, Name = tag.Name };
            }
            else
            {
                return null;
            }
        }

        public async Task<TagResponse> Create(TagRequest tagRequest)
        {

            var tag = new Tag { Name = tagRequest.Name };
            await _context.AddAsync(tag);
            int affected = await _context.SaveChangesAsync();
            if (affected == 1)
            {
                return new TagResponse { Id = tag.Id, Name = tag.Name };

            }
            else
            {
                return null;
            }
        }
        public async Task<TagResponse> Update(Guid id, TagRequest tagRequest)
        {
            var tag = new Tag { Id = tagRequest.Id, Name = tagRequest.Name };
            _context.Entry(tag).State = EntityState.Modified;
            int affected = await _context.SaveChangesAsync();
            if (affected == 1)
            {
                return new TagResponse { Id = tag.Id, Name = tag.Name };
            }
            else
            {
                return null;
            }
        }

        public Task<bool> IsNameExisted(string name)
        {
            return _context.Tags.AnyAsync(t => t.Name.ToUpper() == name.ToUpper());
        }

        public Task<bool> IsIdExisted(Guid id)
        {
            return _context.Tags.AnyAsync(t => t.Id == id);
        }

        public Task<int> SumBlogs(Guid tagId)
        {
            return _context.BlogTags.Where(bt => bt.TagId == tagId).CountAsync();
        }
    }
}
