﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Repositories
{
    public interface IModelRepository<T>
    {
        Task<List<T>> GetAll();
        Task<T> GetById(Guid id);
    }
}
