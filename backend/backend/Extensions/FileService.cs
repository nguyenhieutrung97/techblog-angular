﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.PixelFormats;
using System.Globalization;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace backend.Extensions
{
    public static class FileService
    {
        public static bool CheckAndCreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                return true;
            }
            return false;
        }
        public static bool CheckAndDeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path,recursive:true);
                return true;
            }
            return false;
        }
        public static async Task<string> UploadSingleImage(string webRootPath, IFormFile imageData, string typeObject, string objectId, string fileName)
        {
            string fileStoreImage = "", filePath = "";
            CheckAndCreateDirectory($"{ webRootPath}\\images\\{ typeObject}");
            // Saving Image on Server
            if (imageData!=null&&imageData.Length > 0)
            {
                fileStoreImage = $"{ webRootPath}\\images\\{ typeObject}\\{ objectId}";
                Directory.CreateDirectory(fileStoreImage);
                filePath = Path.Combine(fileStoreImage, fileName + ".jpg");
                using (var stream = System.IO.File.Create(filePath))
                {
                    await imageData.CopyToAsync(stream).ConfigureAwait(false);
                }
            }
            //return filePath;
            string urlHost = $"/images/{ typeObject}/{ objectId}/{fileName}.jpg";
            return urlHost;
        }
        public static async Task<string> UploadSingleImageOfBlog(string webRootPath, IFormFile imageData,string emailUser,string folderName,string imageName)
        {

            string fileStoreImage = "", filePath = "";
            CheckAndCreateDirectory($"{ webRootPath}\\images\\blogs\\ckeditor5");
            CheckAndCreateDirectory($"{ webRootPath}\\images\\blogs\\ckeditor5\\{emailUser}");
            CheckAndCreateDirectory($"{ webRootPath}\\images\\blogs\\ckeditor5\\{emailUser}\\{folderName}");
            string folderImage = DateTime.Now.ToString("yyyyMMddHHmmss",CultureInfo.CurrentCulture);
            // Saving Image on Server
            if (imageData!=null&&imageData.Length > 0)
            {
                fileStoreImage = $"{ webRootPath}\\images\\blogs\\ckeditor5\\{emailUser}\\{folderName}\\{folderImage}";
                Directory.CreateDirectory(fileStoreImage);
                filePath = Path.Combine(fileStoreImage, imageName + ".jpg");
                using (var stream = System.IO.File.Create(filePath))
                {
                    await imageData.CopyToAsync(stream).ConfigureAwait(false);
                }
            }
            //return filePath;
            string urlHost = $"/images/blogs/ckeditor5/{emailUser}/{folderName}/{folderImage}/{imageName}.jpg";
            return urlHost;
        }
        public static void ResizeImage(string inputPath, string outputPath, int width, int height)
        {
            // Image.Load(string path) is a shortcut for our default type. 
            // Other pixel formats use Image.Load<TPixel>(string path))
            using (Image image = Image.Load(inputPath))
            {
                if (height == 0 )
                {
                    height = (width/image.Width)* image.Height;
                }
                if(width==0)
                {
                    width = (height/image.Height) * image.Width;
                }
                image.Mutate(x => x
                     .Resize(width,height));
                image.Save(outputPath); // Automatic encoder selected based on extension.
            }
        }

    }
}
