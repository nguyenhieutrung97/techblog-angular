﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Repositories;
using backend.Models.ViewModels.RequestModels;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        private readonly ITagRepository _tagRepository;

        public TagsController(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }
        [AllowAnonymous]
        // GET: api/Tags
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagResponse>>> GetTags()
        {
            return Ok(await _tagRepository.GetAll().ConfigureAwait(false));
        }
        [AllowAnonymous]
        // GET: api/Tags/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TagResponse>> GetTag(Guid id)
        {
            var tag = await _tagRepository.GetById(id).ConfigureAwait(false);

            if (tag == null)
            {
                return NotFound();
            }

            return Ok(tag);
        }
        [HttpGet("{tagId}/blog-sum")]
        public async Task<ActionResult<int>> GetSumBlogsOfTag(Guid tagId)
        {
            return Ok(await _tagRepository.SumBlogs(tagId).ConfigureAwait(false));
        }

        [Authorize(Roles = "Admin")]
        // DELETE: api/Tags/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TagResponse>> DeleteTag(Guid id)
        {
            var tag = await _tagRepository.Delete(id).ConfigureAwait(false);
            if (tag == null)
            {
                return NotFound();
            }
            return Ok(tag);
        }

        [Authorize(Roles = "Admin")]
        // POST: api/Tags
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TagResponse>> PostTag(TagRequest tagRequest)
        {
            if(!ModelState.IsValid || await _tagRepository.IsNameExisted(tagRequest.Name).ConfigureAwait(false))
            {
                return BadRequest("Tag name existed!!!");
            }
            var tag = await _tagRepository.Create(tagRequest).ConfigureAwait(false);
            if (tag != null)
            {
                return CreatedAtAction("GetTag", new { id = tag.Id }, tag);
            }
            return BadRequest("Something wrong with server");
        }

        [Authorize(Roles = "Admin")]
        // PUT: api/Tags/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTag(Guid id, TagRequest tagRequest)
        {
            if (id != tagRequest.Id)
            {
                return BadRequest();
            }

            if(await _tagRepository.IsIdExisted(tagRequest.Id).ConfigureAwait(false))
            {
                return NotFound();
            }
            var tag = await _tagRepository.Update(id, tagRequest).ConfigureAwait(false);
            if (tag != null)
            {
                return NoContent();
            }
            else
            {
                return BadRequest("Something wrong with server");
            }

        }
    }
}
