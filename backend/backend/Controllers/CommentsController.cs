﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using backend.Models.ViewModels.RequestModels;
using backend.Models.ViewModels.ResponseModels;
using backend.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IUserRepository _userRepository;
        public CommentsController(ICommentRepository commentRepository, IUserRepository userRepository)
        {
            _commentRepository = commentRepository;
            _userRepository = userRepository;
        }
        [HttpGet]
        public async Task<ActionResult<CommentResponse>> GetComment(Guid id)
        {
            var comment = await _commentRepository.GetById(id).ConfigureAwait(false);
            if(comment != null)
            {
                return Ok(comment);
            }
            return NotFound();
        }
        [HttpGet("blogs/{blogId}")]
        public async Task<ActionResult<List<CommentResponse>>> GetCommentsByBlogId(Guid blogId)
        {

            var commentResponses = await _commentRepository.GetByBlogId(blogId).ConfigureAwait(false);
            return Ok(commentResponses);
        }
        [HttpPost]
        public async Task<ActionResult<CommentResponse>> PostComment(CommentRequest commentRequest)
        {
            ClaimsPrincipal currentUser = this.User;
            var emailUser = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _userRepository.GetByEmail(emailUser).ConfigureAwait(false);
            var comment = await _commentRepository.Create(commentRequest, user.Id, user.ImageUrl, user.NameDisplay).ConfigureAwait(false);
            if (comment != null)
            {
                return CreatedAtAction("GetComment",new { id=comment.Id},comment);
            }
            return BadRequest();
        }
    }
}