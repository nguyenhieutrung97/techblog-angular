﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Models.FakeData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WelcomesController : ControllerBase
    {
        private readonly IMock _mock;
        public WelcomesController(IMock mock)
        {
            _mock = mock;
        }
        [HttpGet]
        public async Task<string> GetWelcomes()
        {
            await _mock.FakeData().ConfigureAwait(false);
            return "Fake data success!!! Admin Account: nguyenhieutrung@gmail.com  Pass: trunghieu  |   User Account: truongtankhanh@gmail.com  Pass: khanhtan";
        }
    }
}