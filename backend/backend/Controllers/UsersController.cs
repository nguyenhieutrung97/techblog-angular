﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Models.EFModels;
using backend.Repositories;
using backend.Models.ViewModels;
using backend.Models.ViewModels.ResponseModels;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;


        public UsersController( IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<TokenResponse>> Login(LoginRequest loginVM)
        {
            TokenResponse tokenResponse = await _userRepository.Login(loginVM).ConfigureAwait(false);
            if (tokenResponse == null)
            {
                return BadRequest();
            }
            return Ok(tokenResponse);
        }
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult<RegisterRequest>> RegisterUser(RegisterRequest registerVM)
        {
            RegisterState registerState = await _userRepository.Register(registerVM).ConfigureAwait(false);
            if (registerState == RegisterState.EmailExist || registerState==RegisterState.Failled)
            {
                return BadRequest();
            }
            return StatusCode(201, registerVM);
        }
        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserResponse>> GetUser(Guid id)
        {
            var user = await _userRepository.GetById(id).ConfigureAwait(false);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
        // GET: api/Users
        [HttpGet("current")]
        public async Task<ActionResult<IEnumerable<UserResponse>>> GetCurrentUser()
        {
            ClaimsPrincipal currentUser = this.User;
            var email = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userResult = await _userRepository.GetByEmail(email).ConfigureAwait(false);
            if (userResult == null)
            {
                return NotFound();
            }
            else 
            {
                return Ok(userResult); 
            }
        }

        
    }
}
