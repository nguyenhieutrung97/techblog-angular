﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Models.EFModels;
using backend.Repositories;
using backend.Models.FakeData;
using backend.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using backend.Models.ViewModels.RequestModels;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BlogsController : ControllerBase
    {

        private readonly IBlogRepository _blogRepository;
        private readonly IUserRepository _userRepository;

        public BlogsController(IBlogRepository blogRepository, IUserRepository userRepository)
        {

            _blogRepository = blogRepository;
            _userRepository = userRepository;
        }

        [AllowAnonymous]
        // GET: api/Blogs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlogDetailResponse>> GetBlog(Guid id)

        {
            BlogDetailResponse blog = await _blogRepository.GetDetailById(id).ConfigureAwait(false);

            if (blog == null)
            {
                return NotFound();
            }

            return Ok(blog);
        }

        [AllowAnonymous]
        [HttpGet("search")]
        public async Task<ActionResult<BlogDetailResponse>> GetBlogsBySearchString([FromQuery]string searchString)
        {
            List<BlogResponse> blogs = null;
            if (!string.IsNullOrEmpty(searchString))
            {
                blogs = await _blogRepository.GetBySearchString(searchString).ConfigureAwait(false);

            }
            return Ok(blogs);
        }

        [AllowAnonymous]
        [HttpGet]
        [HttpGet("tags/{tagName}")]
        public async Task<ActionResult<IEnumerable<BlogResponse>>> GetBlogs(string tagName, [FromQuery] PageParameters pageParameters)
        {
            List<BlogResponse> blogs = null;
            if (pageParameters != null)
            {
                blogs = await _blogRepository.GetByConditionParameters(tagName, pageParameters.PageNumber, pageParameters.PageSize).ConfigureAwait(false);

            }

            return Ok(blogs);
        }

        [HttpPost("images/blog-image/{folderName}")]
        public async Task<ActionResult> PostBlogImage(string folderName, IFormFile upload)
        {
            ClaimsPrincipal currentUser = this.User;
            var emailUser = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            string imageUrl = await _blogRepository.CreateBlogImage(emailUser, folderName, upload).ConfigureAwait(false);

            if (imageUrl != null)
            {
                imageUrl = $"{Request.Scheme}://{Request.Host.Value}{imageUrl}";
                return StatusCode(201, new { url = imageUrl });
            }
            var error = new { message = "The image upload failed because the image was too big (max 1.5MB)." };
            return BadRequest(new
            {
                error
            });
        }
        [HttpDelete("images/blog-image/{folderName}")]
        public async Task<ActionResult> DeleteBlogImage(string folderName)
        {
            ClaimsPrincipal currentUser = this.User;
            var emailUser = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var result = await _blogRepository.DeleteBlogImage(emailUser, folderName).ConfigureAwait(false);

            if (result)
            {
                return NoContent();
            }
            return NotFound();
        }
        [HttpPost("images/display-image/{id}")]
        public async Task<ActionResult> PostDisplayImage(Guid id, IFormFile imageDisplay)
        {
            string imageUrl = await _blogRepository.CreateDisplayImage(id, imageDisplay).ConfigureAwait(false);
            if (imageUrl != null)
            {
                return StatusCode(201, new { imageUrl });
            }
            return BadRequest();
        }

        // POST: api/Blogs
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<BlogDetailResponse>> PostBlog(BlogDetailRequest blog)
        {
            ClaimsPrincipal currentUser = this.User;
            var emailUser = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _userRepository.GetByEmail(emailUser).ConfigureAwait(false);
            BlogDetailResponse blogResponse = await _blogRepository.Create(user.Id, blog).ConfigureAwait(false);
            if (blogResponse != null)
            {
                return CreatedAtAction("GetBlog", new { id = blogResponse.BlogId }, blogResponse); ;
            }
            return BadRequest();
        }
    }
}
