﻿using backend.Controllers;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Models.ViewModels.ResponseModels;
using backend.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace backend.Test
{
    public class UsersControllerTest
    {
        IUserRepository _userRepository;
        UsersController _usersController;
        public UsersControllerTest()
        {
            //Arrange
            var inMemorySettings = new Dictionary<string, string>
            {
                {"JWTStorage:Secret", "nguyenhieutrung.07"}
            };

            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            var options = new DbContextOptionsBuilder<TechBlogContext>().UseSqlServer(@"Server=.;Database=TechBlogDB;Trusted_Connection=True;").Options;
            TechBlogContext context = new TechBlogContext(options);
            _userRepository = new UserRepository(context,configuration);
            _usersController = new UsersController(_userRepository);
        }
        [Fact]
        public async void GetUser_UnknownGuidPassed_ReturnsNotFoundResult()
        {

            // Act
            var controllerResult = await _usersController.GetUser(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetUser_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("3838C61D-54DE-419B-A383-A0D8C6336D3A");

            // Act
            var controllerResult = await _usersController.GetUser(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }

        [Fact]
        public async void GetUser_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("3838C61D-54DE-419B-A383-A0D8C6336D3A");

            // Act
            var controllerResult = await _usersController.GetUser(testGuid);
            var okResult = controllerResult.Result as OkObjectResult;

            // Assert
            Assert.IsType<UserResponse>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as UserResponse).Id);
        }
        [Fact]
        public async void Login_AccountValid_ReturnsOkResult()
        {
            //Arrange
            var accountCorrect = new LoginRequest { Email = "nguyenhieutrung@gmail.com", Password = "trunghieu" };
            // Act
            var controllerResult = await _usersController.Login(accountCorrect);
            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }
        [Fact]
        public async void Login_AccountValid_ReturnsOkResultHasToken()
        {
            //Arrange
            var accountCorrect = new LoginRequest { Email = "nguyenhieutrung@gmail.com", Password = "trunghieu" };
            // Act
            var controllerResult = await _usersController.Login(accountCorrect);
            var okResponse = controllerResult.Result as OkObjectResult;
            var token = okResponse.Value;
            // Assert
            Assert.IsType<TokenResponse>(token);
        }
        [Fact]
        public async void Login_AccountInvalid_ReturnsBadRequest()
        {
            //Arrange
            var accountInvalid = new LoginRequest { Email = "nguyenhieutrung12@gmail.com", Password = "trunghieu" };
            // Act
            var controllerResult = await _usersController.Login(accountInvalid);
            // Assert
            Assert.IsType<BadRequestResult>(controllerResult.Result);
        }
        [Fact]
        public async void Register_ReturnsOkResult()
        {
            //Arrange
            var registerRequest = new RegisterRequest { Email="usertest1@gmail.com",Password="testuser1",NameDisplay="User Test 1" };
            // Act
            var controllerResult = await _usersController.RegisterUser(registerRequest);
            var createdResponse = controllerResult.Result as ObjectResult;
            // Assert
            Assert.Equal(201,createdResponse.StatusCode);
        }
        [Fact]
        public async void Register_ReturnsOkResultHasItem()
        {
            //Arrange
            var registerRequest = new RegisterRequest { Email = "usertest2@gmail.com", Password = "testuser2", NameDisplay = "User Test 2" };
            // Act
            var controllerResult = await _usersController.RegisterUser(registerRequest);
            var createdResponse = controllerResult.Result as ObjectResult;
            var item = createdResponse.Value as RegisterRequest;
            // Assert
            Assert.Equal(registerRequest.Email, item.Email);
        }
        [Fact]
        public async void Register_EmailAlreadyExist_ReturnsBadRequest()
        {
            //Arrange
            var registerRequest = new RegisterRequest { Email = "nguyenhieutrung@gmail.com", Password = "testuser", NameDisplay = "User Test" };
            // Act
            var controllerResult = await _usersController.RegisterUser(registerRequest);
            // Assert
            Assert.IsType<BadRequestResult>(controllerResult.Result);
        }
    }
}
