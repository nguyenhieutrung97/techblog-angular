﻿using backend.Controllers;
using backend.Models.EFModels;
using backend.Models.ViewModels.RequestModels;
using backend.Models.ViewModels.ResponseModels;
using backend.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Xunit;

namespace backend.Test
{

    public class CommentsControllerTest
    {
        IUserRepository _userRepository;
        UsersController _usersController;
        ICommentRepository _commentRepository;
        CommentsController _commentsController;
        public CommentsControllerTest()
        {
            //Arrange
            var inMemorySettings = new Dictionary<string, string>
            {
                {"JWTStorage:Secret", "nguyenhieutrung.07"}
            };

            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            var options = new DbContextOptionsBuilder<TechBlogContext>().UseSqlServer(@"Server=.;Database=TechBlogDB;Trusted_Connection=True;").Options;
            TechBlogContext context = new TechBlogContext(options);
            _userRepository = new UserRepository(context, configuration);
            _usersController = new UsersController(_userRepository);
            _commentRepository = new CommentRepository(context);
            _commentsController = new CommentsController(_commentRepository, _userRepository);
        }
        [Fact]
        public async void GetComment_UnknownGuidPassed_ReturnsNotFoundResult()
        {

            // Act 
            var controllerResult = await _commentsController.GetComment(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetComment_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("67F489DC-2932-4222-B7B8-29EF1C93B635");

            // Act
            var controllerResult = await _commentsController.GetComment(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }

        [Fact]
        public async void GetComment_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("67F489DC-2932-4222-B7B8-29EF1C93B635");

            // Act
            var controllerResult = await _commentsController.GetComment(testGuid);
            var okResult = controllerResult.Result as OkObjectResult;

            // Assert
            Assert.IsType<CommentResponse>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as CommentResponse).Id);
        }

        [Fact]
        public async void GetCommentsByBlogId_ReturnsOkResult()
        {
            Guid blogId = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe");
            // Act
            var controllerResult = await _commentsController.GetCommentsByBlogId(blogId);
            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetCommentsByBlogId_ReturnsAllItemsInBlog()
        {
            Guid blogId = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe");
            // Act
            var controllerResult = await _commentsController.GetCommentsByBlogId(blogId);
            var okResult = controllerResult.Result as OkObjectResult;
            // Assert
            var items = Assert.IsType<List<CommentResponse>>(okResult.Value);
            Assert.True(items.Count > 0);
        }

        [Fact]
        public async void PostComment_ValidObjectPassed_ReturnsCreatedResponse()
        {
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                        {
                        new Claim(ClaimTypes.NameIdentifier, "truongtankhanh@gmail.com")
                        })));
            // Arrange
            var testItem = new CommentRequest()
            {
                BlogId = new Guid("44a731c0-6c0c-4b29-aee3-b3ddbb651496"),CommentContent="hohohohoho"
            };

            // Act
            _commentsController.ControllerContext.HttpContext = contextMock.Object;
            var controllerResult = await _commentsController.PostComment(testItem);
            var createdResponse = controllerResult.Result;

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }


    }
}
