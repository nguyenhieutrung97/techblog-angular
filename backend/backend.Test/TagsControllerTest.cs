using backend.Controllers;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Models.ViewModels.RequestModels;
using backend.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Xunit;

namespace backend.Test
{
    public class TagsControllerTest
    {
        ITagRepository _tagRepository;
        TagsController _tagsController;
        public TagsControllerTest()
        {
            var options = new DbContextOptionsBuilder<TechBlogContext>().UseSqlServer(@"Server=.;Database=TechBlogDB;Trusted_Connection=True;").Options;
            TechBlogContext context = new TechBlogContext(options);
            _tagRepository = new TagRepository(context);
            _tagsController = new TagsController(_tagRepository);
        }
        [Fact]
        public async void GetTags_ReturnsOkResult()
        {

            // Act
            var controllerResult = await _tagsController.GetTags();
            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetTags_ReturnsAllItems()
        {

            // Act
            var controllerResult = await _tagsController.GetTags();
            var okResult = controllerResult.Result as OkObjectResult;
            // Assert
            var items = Assert.IsType<List<TagResponse>>(okResult.Value);
            Assert.True(items.Count > 0);
        }
        [Fact]
        public async void GetTag_UnknownGuidPassed_ReturnsNotFoundResult()
        {

            // Act
            var controllerResult = await _tagsController.GetTag(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetTag_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("77091F52-28D0-4C8F-8CBA-248026707E8A");

            // Act
            var controllerResult = await _tagsController.GetTag(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }

        [Fact]
        public async void GetTag_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("77091F52-28D0-4C8F-8CBA-248026707E8A");

            // Act
            var controllerResult = await _tagsController.GetTag(testGuid);
            var okResult = controllerResult.Result as OkObjectResult;

            // Assert
            Assert.IsType<TagResponse>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as TagResponse).Id);
        }

        [Fact]
        public async void PostTag_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var nameMissingItem = new TagRequest()
            {
            };
            _tagsController.ModelState.AddModelError("Name", "Required");

            // Act
            var controllerResult = await _tagsController.PostTag(nameMissingItem);
            var badResponse = controllerResult.Result;

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }
        [Fact]
        public async void PostTag_DuplicateNameObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var duplicateNameItem = new TagRequest()
            {
                Name = "beginners"
            };

            // Act
            var controllerResult = await _tagsController.PostTag(duplicateNameItem);
            var badResponse = controllerResult.Result;

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public async void PostTag_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            var testItem = new TagRequest()
            {
                Name = "vuejs"
            };

            // Act
            var controllerResult = await _tagsController.PostTag(testItem);
            var createdResponse = controllerResult.Result;

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }


        [Fact]
        public async void PostTag_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new TagRequest()
            {
                Name = "golang"
            };

            // Act
            var controllerResult = await _tagsController.PostTag(testItem);
            var createdResponse = controllerResult.Result as CreatedAtActionResult;
            var item = createdResponse.Value as TagResponse;

            // Assert
            Assert.IsType<TagResponse>(item);
            Assert.Equal("golang", item.Name);
        }
        [Fact]
        public async void DeleteTag_NotExistingGuidPassed_ReturnsNotFoundResponse()
        {
            // Arrange
            var notExistingGuid = Guid.NewGuid();

            // Act
            var controllerResult = await _tagsController.DeleteTag(notExistingGuid);
            var badResponse = controllerResult.Result;

            // Assert
            Assert.IsType<NotFoundResult>(badResponse);
        }

        [Fact]
        public async void DeleteTag_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var existingGuid = new Guid("FA392B70-5EF0-487A-98E6-9A51314FE252");

            // Act
            var controllerResult = await _tagsController.DeleteTag(existingGuid);
            var okResponse = controllerResult.Result;

            // Assert
            Assert.IsType<OkObjectResult>(okResponse);
        }
        [Fact]
        public async void DeleteTag_ExistingGuidPassed_RemovesOneItem()
        {
            var preTags = await _tagRepository.GetAll();
            // Arrange
            var existingGuid = new Guid("886961D3-CF19-4979-815C-C1EE600F2063");

            // Act
            var controllerResult = await _tagsController.DeleteTag(existingGuid);
            var okResponse = controllerResult.Result;
            var tags = await _tagRepository.GetAll();
            // Assert
            Assert.Equal(preTags.Count - 1, tags.Count);
        }

        //[Fact]
        //public async void Test1()
        //{
        //    var fake = new List<TagResponse>() {
        //        new TagResponse { Id = new Guid("77091F52-28D0-4C8F-8CBA-248026707E8A"), Name = "beginners" },
        //         new TagResponse { Id = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54"), Name = "csharp"},
        //        new TagResponse { Id = new Guid("692AF7F8-8440-424A-A1EA-051C3E69E88D"), Name = "css" },
        //         new TagResponse { Id = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9"), Name = "dotnet"},
        //         new TagResponse { Id = new Guid("19A08140-724B-4572-B5EA-E781154983F9"), Name = "html"},
        //          new TagResponse { Id = new Guid("D3E04309-565C-4473-B7DD-59C3E3F3827B"), Name = "javascript" },
        //          new TagResponse {Id = new Guid("FA392B70-5EF0-487A-98E6-9A51314FE252"), Name = "node" },
        //           new TagResponse { Id = new Guid("886961D3-CF19-4979-815C-C1EE600F2063"), Name = "react"},
        //           new TagResponse { Id = new Guid("74A34FFA-CDD5-4496-941E-5B03D4E4C674"), Name = "webdev" },
        //    };
        //    Assert.False(false,"Get all tag data invalid");
        //}
    }
}
