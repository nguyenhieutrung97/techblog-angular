﻿using backend.Controllers;
using backend.Models.EFModels;
using backend.Models.ViewModels;
using backend.Models.ViewModels.RequestModels;
using backend.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Xunit;

namespace backend.Test
{
    public class BlogsControllerTest
    {
        IUserRepository _userRepository;
        UsersController _usersController;
        IBlogRepository _blogRepository;
        BlogsController _blogsController;
        public BlogsControllerTest()
        {
            //Arrange
            var inMemorySettings = new Dictionary<string, string>
            {
                {"JWTStorage:Secret", "nguyenhieutrung.07"}
            };

            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            var options = new DbContextOptionsBuilder<TechBlogContext>().UseSqlServer(@"Server=.;Database=TechBlogDB;Trusted_Connection=True;").Options;
            TechBlogContext context = new TechBlogContext(options);
            _userRepository = new UserRepository(context, configuration);
            _usersController = new UsersController(_userRepository);

            var mockEnvironment = new Mock<IWebHostEnvironment>();
            //...Setup the mock as needed
            mockEnvironment
                .Setup(m => m.WebRootPath)
                .Returns("");
            _blogRepository = new BlogRepository(context, mockEnvironment.Object);
            _blogsController = new BlogsController(_blogRepository, _userRepository);
        }
        [Fact]
        public async void GetBlog_UnknownGuidPassed_ReturnsNotFoundResult()
        {

            // Act
            var controllerResult = await _blogsController.GetBlog(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetBlog_ExistingGuidPassed_ReturnsOkResult()
        {
            // Arrange
            var testGuid = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe");

            // Act
            var controllerResult = await _blogsController.GetBlog(testGuid);

            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }

        [Fact]
        public async void GetBlog_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var testGuid = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe");

            // Act
            var controllerResult = await _blogsController.GetBlog(testGuid);
            var okResult = controllerResult.Result as OkObjectResult;

            // Assert
            Assert.IsType<BlogDetailResponse>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as BlogDetailResponse).BlogId);
        }

        [Fact]
        public async void GetBlogs_ReturnsOkResult()
        {

            // Act
            var controllerResult = await _blogsController.GetBlogs("csharp", new PageParameters());
            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetBlogs_ReturnsAllItems()
        {

            // Act
            var controllerResult = await _blogsController.GetBlogs("csharp", new PageParameters());
            var okResult = controllerResult.Result as OkObjectResult;
            // Assert
            var items = Assert.IsType<List<BlogResponse>>(okResult.Value);
            Assert.True(items.Count > 0);
        }

        [Fact]
        public async void GetBlogsBySearchString_ReturnsOkResult()
        {

            // Act
            var controllerResult = await _blogsController.GetBlogsBySearchString("n");
            // Assert
            Assert.IsType<OkObjectResult>(controllerResult.Result);
        }
        [Fact]
        public async void GetBlogsBySearchString_ReturnsAllItems()
        {

            // Act
            var controllerResult = await _blogsController.GetBlogsBySearchString("n");
            var okResult = controllerResult.Result as OkObjectResult;
            // Assert
            var items = Assert.IsType<List<BlogResponse>>(okResult.Value);
            Assert.True(items.Count > 0);
        }

        [Fact]
        public async void PostBlog_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var testItem = new BlogDetailRequest()
            {
                HtmlContent = "<h1>PostBlog_ValidObjectPassed_ReturnsCreatedResponse</h1>",
                Tags = new List<TagResponse> { new TagResponse { Id = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9"), Name = "dotnet" }, new TagResponse { Id = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54"), Name = "csharp" } }
            };
            _blogsController.ModelState.AddModelError("Title", "Required");
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                        {
                        new Claim(ClaimTypes.NameIdentifier, "truongtankhanh@gmail.com")
                        })));
            // Act
            _blogsController.ControllerContext.HttpContext = contextMock.Object;
            var controllerResult = await _blogsController.PostBlog(testItem);
            var badResponse = controllerResult.Result;
            // Assert
            Assert.IsType<BadRequestResult>(badResponse);
        }

        [Fact]
        public async void PostBlog_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            var testItem = new BlogDetailRequest()
            {
                Title = "PostBlog_ValidObjectPassed_ReturnsCreatedResponse",
                HtmlContent = "<h1>PostBlog_ValidObjectPassed_ReturnsCreatedResponse</h1>",
                Tags = new List<TagResponse> { new TagResponse { Id = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9"), Name = "dotnet" }, new TagResponse { Id = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54"), Name = "csharp" } }
            };
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                        {
                        new Claim(ClaimTypes.NameIdentifier, "truongtankhanh@gmail.com")
                        })));
            // Act
            _blogsController.ControllerContext.HttpContext = contextMock.Object;
            var controllerResult = await _blogsController.PostBlog(testItem);
            var createdResponse = controllerResult.Result;

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }


        [Fact]
        public async void PostBlog_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new BlogDetailRequest()
            {
                Title = "PostBlog_ValidObjectPassed_ReturnedResponseHasCreatedItem",
                HtmlContent = "<h1>PostBlog_ValidObjectPassed_ReturnedResponseHasCreatedItem</h1>",
                Tags = new List<TagResponse> { new TagResponse { Id = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9"), Name = "dotnet" }, new TagResponse { Id = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54"), Name = "csharp" } }
            };
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                        {
                        new Claim(ClaimTypes.NameIdentifier, "truongtankhanh@gmail.com")
                        })));
            // Act
            _blogsController.ControllerContext.HttpContext = contextMock.Object;
            var controllerResult = await _blogsController.PostBlog(testItem);
            var createdResponse = controllerResult.Result as CreatedAtActionResult;
            var item = createdResponse.Value as BlogDetailResponse;
            // Assert
            Assert.IsType<BlogDetailResponse>(item);
            Assert.Equal(testItem.Title, item.Title);
        }
    }
}
