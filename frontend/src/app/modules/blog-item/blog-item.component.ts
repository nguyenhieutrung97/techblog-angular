import { environment } from './../../../environments/environment.prod';
import { Component, OnInit, Input } from '@angular/core';
import { Blog } from 'src/app/data/schema/blog';

@Component({
  selector: 'blog-item',
  templateUrl: './blog-item.component.html',
  styleUrls: ['./blog-item.component.css'],
})
export class BlogItemComponent implements OnInit {
  @Input('blog') blog: Blog;
  urlHost = environment.backendUrl;
  constructor() {}

  ngOnInit(): void {}
}
