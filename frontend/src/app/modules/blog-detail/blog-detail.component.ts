import { BadRequestError } from '../../shared/service/error-handler/errror-handler-schema/badrequest-error-model';
import { BlogService } from 'src/app/data/service/blog/blog.service';
import { BlogDetail } from './../../data/schema/blog-detail';
import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ErrorModel } from 'src/app/shared/service/error-handler/errror-handler-schema/error-model';
import { NotFoundError } from 'src/app/shared/service/error-handler/errror-handler-schema/notfound-error-model';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
declare var hljs: any;
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css'],
})
export class BlogDetailComponent implements OnInit, AfterViewChecked {
  blogDetail: BlogDetail = new BlogDetail();
  urlHost = environment.backendUrl;
  currentBlogId: string;
  constructor(
    private blogService: BlogService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.loadBlogDetail(params.get('id'));
      this.currentBlogId = params.get('id');
    });
  }
  ngAfterViewChecked() {
    console.log('ngAfterViewChecked:');
    if (this.blogDetail.htmlContent !== '') {
      this.callHighLight();
    }
  }
  callHighLight() {
    document.querySelectorAll('pre code').forEach((block) => {
      hljs.highlightBlock(block);
    });
  }
  loadBlogDetail(id: string) {
    this.blogService.getBlogById(id).subscribe(
      (response) => {
        this.blogDetail = { ...response };
      },
      (error: ErrorModel) => {
        if (
          error instanceof NotFoundError ||
          error instanceof BadRequestError
        ) {
          this.blogDetail.blogId = null;
          this.toastr.warning('Blog does not exists');
        }
      }
    );
  }
}
