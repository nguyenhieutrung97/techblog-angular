import { Component, OnInit, DoCheck } from '@angular/core';
import { Blog } from '../../data/schema/blog';
import { BlogService } from 'src/app/data/service/blog/blog.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { NotFoundError } from 'src/app/shared/service/error-handler/errror-handler-schema/notfound-error-model';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  blogs: Blog[] = [];
  pageNumber = 1;
  pageSize = 4;
  currentTag: string = null;
  loadMoreAvailable = true;
  loaded = false;
  constructor(
    private blogService: BlogService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.route.queryParamMap.subscribe((queryVals) => {
      if (queryVals.get('pageNumber')) {
        this.pageNumber = parseInt(queryVals.get('pageNumber'), 10);
      }
      if (queryVals.get('pageSize')) {
        this.pageSize = parseInt(queryVals.get('pageSize'), 10);
      }
      if (queryVals.get('searchString')) {
        this.loadBlogsBySearchString(queryVals.get('searchString'));
      } else {
        this.route.paramMap.subscribe((params) => {
          // recall when it change tag so we need initialize pageNum
          this.pageNumber = 1;
          this.currentTag = params.get('tagName');
          this.loadBlogs(params.get('tagName'), true);
        });
      }
    });

    console.log('Home init');
  }
  responseHandling(response, initState) {
    if (initState) {
      this.blogs = [...response];
      this.loadMoreAvailable = true;
    } else {
      this.blogs = [...this.blogs.concat(response)];
    }
    if (response.length < this.pageSize) {
      this.loadMoreAvailable = false;
    }
  }
  errorHandling(error) {
    if (error instanceof BadRequestError) {
      this.toastr.error('Server overloading!!! Try later!!!');
    } else if (error instanceof NotFoundError) {
      this.router.navigate(['/notfound']);
    }
  }
  loadBlogs(tagName?: string, initState?: boolean) {
    if (tagName === null) {
      this.blogService.getBlogs(this.pageNumber, this.pageSize).subscribe(
        (response) => {
          this.responseHandling(response, initState);
        },
        (error) => {
          this.errorHandling(error);
        },
        () => {
          this.loaded = true;
        }
      );
    } else {
      this.blogService
        .getBlogByTagName(tagName, this.pageNumber, this.pageSize)
        .subscribe(
          (response) => {
            this.responseHandling(response, initState);
          },
          (error) => {
            this.errorHandling(error);
          },
          () => {
            this.loaded = true;
          }
        );
    }
  }
  loadBlogsBySearchString(searchString: string) {
    this.blogService.getBlogBySearchString(searchString).subscribe(
      (response) => {
        this.blogs = [...response];
        this.loadMoreAvailable = false;
      },
      (error) => {
        this.errorHandling(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }
  onClickLoadMore() {
    this.pageNumber++;
    this.loadBlogs(this.currentTag, false);
  }
}
