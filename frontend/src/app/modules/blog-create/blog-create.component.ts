import { AuthenticationService } from './../../data/service/auth/authentication.service';
import { BlogDetail } from './../../data/schema/blog-detail';
import { BlogService } from 'src/app/data/service/blog/blog.service';
import { BlogCreate } from './../../data/schema/blog-create';
import { TagService } from 'src/app/data/service/tag/tag.service';
import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Tag } from 'src/app/data/schema/tag';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as Editor from '../../../assets/ckeditor5/build/ckeditor';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-blog-create',
  templateUrl: './blog-create.component.html',
  styleUrls: ['./blog-create.component.css'],
})
export class BlogCreateComponent implements OnInit, OnDestroy {
  urlHost = environment.backendUrl;
  tagsSelect: Tag[];
  currentImage: File;
  folderStoreImages = Date.now();
  isBlogCreated = false;
  public Editor = Editor;
  config = {
    toolbar: [
      'heading',
      '|',
      'bold',
      'italic',
      'link',
      'bulletedList',
      'numberedList',
      '|',
      'indent',
      'outdent',
      'alignment',
      '|',
      'codeBlock',
      'imageUpload',
      'blockQuote',
      'insertTable',
      'mediaEmbed',
      'undo',
      'redo',
    ],
    indentBlock: {
      offset: 1,
      unit: 'em',
    },
    image: {
      toolbar: ['imageTextAlternative', 'imageStyle:full', 'imageStyle:side'],
    },
    table: {
      contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
    },
    simpleUpload: null,
  };
  constructor(
    private el: ElementRef,
    private tagService: TagService,
    private blogService: BlogService,
    private toastr: ToastrService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.tagService.getTags().subscribe((response) => {
      this.tagsSelect = response;
    });
    this.authenticationService.currentUser.subscribe((user) => {
      this.config.simpleUpload = {
        // The URL that the images are uploaded to.
        uploadUrl: `${this.urlHost}/api/blogs/images/blog-image/${this.folderStoreImages}`,

        // Headers sent along with the XMLHttpRequest to the upload server.
        headers: {
          'X-CSRF-TOKEN': 'CSFR-Token',
          Authorization: `Bearer ${user.token}`,
        },
      };
    });
  }
  ngOnDestroy() {
    if (!this.isBlogCreated) {
      this.blogService.deleteImageBlog(this.folderStoreImages).subscribe();
    }
  }
  createImageDisplay(blogId, imageDisplay, titleBlog) {
    this.blogService.createImageDisplay(blogId, imageDisplay).subscribe(
      (response: string) => {
        this.toastr.success(`Blog ${titleBlog} created successful!!!`);
        this.isBlogCreated = true;
        this.router.navigate(['/']);
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.warning('Image display create fail!!!');
        } else {
          this.toastr.error(error);
        }
      }
    );
  }
  createBlog(blogObject) {
    const blog = new BlogCreate(
      blogObject.title,
      blogObject.htmlContent,
      blogObject.tags
    );
    this.blogService.createBlog(blog).subscribe(
      (response: BlogDetail) => {
        this.createImageDisplay(
          response.blogId,
          this.currentImage,
          response.title
        );
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.warning('Blog create fail!!!');
        } else {
          this.toastr.error(error);
        }
      }
    );
  }
  onChangeImage(event) {
    this.currentImage = event.target.files[0];
  }
  onSubmit(form) {
    if (form.valid) {
      this.createBlog(form.value);
    } else {
      const firstInvalidControl = this.el.nativeElement.querySelector(
        '#blog-create-form .form-group .ng-invalid'
      );
      if (firstInvalidControl) {
        firstInvalidControl.focus();
      }
    }
  }
}
