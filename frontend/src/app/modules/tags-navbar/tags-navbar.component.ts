import { switchMap, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Tag } from 'src/app/data/schema/tag';
import { TagService } from 'src/app/data/service/tag/tag.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';

@Component({
  selector: 'app-tags-navbar',
  templateUrl: './tags-navbar.component.html',
  styleUrls: ['./tags-navbar.component.css'],
})
export class TagsNavbarComponent implements OnInit {
  initialTag = new Tag('', 'all');
  tags: Tag[];

  constructor(
    private tagService: TagService,
    private toastr: ToastrService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.loadTags();
  }
  addInitialTagToTags(tags: Tag[]): Tag[] {
    return [this.initialTag].concat(tags);
  }
  loadTags() {
    this.tagService.getTags().subscribe(
      (response) => {
        this.tags = [...this.addInitialTagToTags(response)];
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.error('Server overloading!!! Try later!!!');
        } else {
          this.router.navigate(['/severerror']);
        }
      }
    );
  }
}
