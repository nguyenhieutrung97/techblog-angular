import { CommentService } from './../../data/service/comment/comment.service';
import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Comment } from 'src/app/data/schema/comment';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/data/service/auth/authentication.service';
import { CommentCreate } from 'src/app/data/schema/comment-create';

@Component({
  selector: 'app-comment-section',
  templateUrl: './comment-section.component.html',
  styleUrls: ['./comment-section.component.css'],
})
export class CommentSectionComponent implements OnInit {
  @Input('blogId') blogId: string;
  comments: Comment[];
  nameCurrentUser = '';
  urlHost = environment.backendUrl;
  constructor(
    private el: ElementRef,
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private commentService: CommentService
  ) {}

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe((user) => {
      if (user != null) {
        this.nameCurrentUser = user.nameDisplay;
      }
    });
    this.commentService
      .getCommentsByBlogId(this.blogId)
      .subscribe((response) => {
        this.comments = [...response];
      });
  }
  createComment(form) {
    this.commentService
      .createComment(new CommentCreate(this.blogId, form.value.commentContent))
      .subscribe(
        (response) => {
          this.comments.unshift(response);
          form.reset();
        },
        (error) => {
          if (error instanceof BadRequestError) {
            this.toastr.warning('Comment create fail!!!');
          } else {
            this.toastr.error(error);
          }
        }
      );
  }
  onSubmit(form) {
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser) {
      // logged in so return true
      if (form.valid) {
        this.createComment(form);
      } else {
        const firstInvalidControl = this.el.nativeElement.querySelector(
          '#comment-form textarea.ng-invalid'
        );
        if (firstInvalidControl) {
          firstInvalidControl.focus();
        }
      }
    } else {
      this.toastr.info('Please log in!!!');
    }
  }
}
