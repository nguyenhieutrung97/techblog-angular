import { Comment } from './../../../data/schema/comment';
import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.css'],
})
export class CommentItemComponent implements OnInit {
  urlHost = environment.backendUrl;
  @Input('comment') comment: Comment;
  constructor() {}

  ngOnInit(): void {}
}
