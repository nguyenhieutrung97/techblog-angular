import { Component, OnInit } from '@angular/core';
import { TagService } from 'src/app/data/service/tag/tag.service';
import { Tag } from 'src/app/data/schema/tag';
import { ToastrService } from 'ngx-toastr';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tags-manager',
  templateUrl: './tags-manager.component.html',
  styleUrls: ['./tags-manager.component.css'],
})
export class TagsManagerComponent implements OnInit {
  constructor(
    private tagService: TagService,
    private toastr: ToastrService,
    private router: Router
  ) {}
  tags: Tag[];
  bindingTag: Tag = null;
  ngOnInit(): void {
    this.loadTags();
  }
  onCreated(tag: Tag) {
    this.tags.unshift(tag);
  }
  onDeleted(tag: Tag) {
    this.tags = [...this.tags.filter((t) => t.id !== tag.id)];
  }
  bindTag(tag: Tag) {
    this.bindingTag = { ...tag };
  }
  loadTags() {
    this.tagService.getTags().subscribe(
      (response) => {
        this.tags = [...response];
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.error('Server overloading!!! Try later!!!');
        } else {
          this.router.navigate(['/severerror']);
        }
      }
    );
  }
}
