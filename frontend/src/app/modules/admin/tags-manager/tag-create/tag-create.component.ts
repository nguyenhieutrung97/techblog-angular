import { Tag } from './../../../../data/schema/tag';
import { TagService } from 'src/app/data/service/tag/tag.service';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';

@Component({
  selector: 'tag-create',
  templateUrl: './tag-create.component.html',
  styleUrls: ['./tag-create.component.css'],
})
export class TagCreateComponent implements OnInit {
  constructor(
    private el: ElementRef,
    private tagService: TagService,
    private toastr: ToastrService
  ) {}
  @Output() created = new EventEmitter<Tag>();
  ngOnInit(): void {}
  createTag(form) {
    this.tagService.createTag(form.value).subscribe(
      (response) => {
        this.created.emit(response);
        form.reset();
        this.toastr.success('Tag created successful!!!');
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.error(
            'Tag created fail. Please check unique name tag!!!'
          );
        }
      }
    );
  }
  onSubmit(form) {
    if (form.valid) {
      this.createTag(form);
    } else {
      const firstInvalidControl = this.el.nativeElement.querySelector(
        '#create-modal input.ng-invalid'
      );
      if (firstInvalidControl) {
        firstInvalidControl.focus();
      }
    }
  }
}
