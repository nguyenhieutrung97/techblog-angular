import { NotFoundError } from './../../../../shared/service/error-handler/errror-handler-schema/notfound-error-model';
import { TagService } from 'src/app/data/service/tag/tag.service';
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { Tag } from 'src/app/data/schema/tag';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'tag-delete',
  templateUrl: './tag-delete.component.html',
  styleUrls: ['./tag-delete.component.css'],
})
export class TagDeleteComponent implements OnChanges {
  @Input() tag: Tag;
  @Output() deleted = new EventEmitter<Tag>();
  sumBlogs: number;

  constructor(private tagService: TagService, private toastr: ToastrService) {}

  ngOnChanges() {
    this.tagService.getSumBlogsOfTag(this.tag.id).subscribe((response) => {
      this.sumBlogs = response;
    });
  }
  onClick(id) {
    this.tagService.deleteTag(id).subscribe(
      (response) => {
        this.deleted.emit(response);
        this.toastr.success(`Tag ${response.name} deleted successful!!!`);
      },
      (error) => {
        if (error instanceof NotFoundError) {
          this.toastr.error('Cant find this tag');
        }
      }
    );
  }
}
