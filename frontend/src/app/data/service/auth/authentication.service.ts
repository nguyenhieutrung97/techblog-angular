import { TokenResponse } from './../../schema/token-response';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginModel } from '../../schema/login-model';
import { environment } from 'src/environments/environment';
import { ErrorHandlerService } from 'src/app/shared/service/error-handler/app-error-handler';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService extends ErrorHandlerService {
  private url = `${environment.backendUrl}/api/users`;
  private currentUserSubject: BehaviorSubject<TokenResponse>;
  currentUser: Observable<TokenResponse>;

  constructor(private http: HttpClient) {
    super();
    this.currentUserSubject = new BehaviorSubject<TokenResponse>(
      JSON.parse(localStorage.getItem('tokenResponse'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  get currentUserValue(): TokenResponse {
    return this.currentUserSubject.value;
  }

  login(login: LoginModel): Observable<TokenResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http
      .post<TokenResponse>(
        this.url + '/login',
        JSON.stringify(login),
        httpOptions
      )
      .pipe(catchError(this.handleError))
      .pipe(
        map((response: TokenResponse) => {
          localStorage.setItem('tokenResponse', JSON.stringify(response));
          this.currentUserSubject.next(response);
          return response;
        })
      );
  }
  logout() {
    localStorage.removeItem('tokenResponse');
    this.currentUserSubject.next(null);
  }
}
