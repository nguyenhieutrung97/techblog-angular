import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tag } from '../../schema/tag';
import { HttpClient } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ErrorHandlerService } from 'src/app/shared/service/error-handler/app-error-handler';

@Injectable({
  providedIn: 'root',
})
export class TagService extends ErrorHandlerService {
  private url = `${environment.backendUrl}/api/tags`;
  constructor(private http: HttpClient) {
    super();
  }

  getTags(): Observable<Tag[]> {
    return this.http
      .get<Tag[]>(this.url)
      .pipe(retry(1), catchError(this.handleError));
  }
  getSumBlogsOfTag(tagId): Observable<number> {
    return this.http
      .get<number>(`${this.url}/${tagId}/blog-sum`)
      .pipe(retry(1), catchError(this.handleError));
  }
  createTag(tag: Tag): Observable<Tag> {
    return this.http
      .post<Tag>(this.url, tag)
      .pipe(catchError(this.handleError));
  }
  deleteTag(id: string): Observable<Tag> {
    return this.http
      .delete<Tag>(`${this.url}/${id}`)
      .pipe(catchError(this.handleError));
  }
}
