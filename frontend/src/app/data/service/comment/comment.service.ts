import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ErrorHandlerService } from 'src/app/shared/service/error-handler/app-error-handler';
import { Comment } from '../../schema/comment';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { CommentCreate } from '../../schema/comment-create';

@Injectable({
  providedIn: 'root',
})
export class CommentService extends ErrorHandlerService {
  private url = `${environment.backendUrl}/api/comments`;
  constructor(private http: HttpClient) {
    super();
  }
  getCommentsByBlogId(blogId: string): Observable<Comment[]> {
    return this.http
      .get<Comment[]>(`${this.url}/blogs/${blogId}`)
      .pipe(retry(3), catchError(this.handleError));
  }
  createComment(comment: CommentCreate): Observable<Comment> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http
      .post<Comment>(this.url, comment, httpOptions)
      .pipe(catchError(this.handleError));
  }
}
