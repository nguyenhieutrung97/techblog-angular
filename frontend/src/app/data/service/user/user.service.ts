import { LoginModel } from './../../schema/login-model';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { RegisterModel } from '../../schema/register-model';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ErrorHandlerService } from 'src/app/shared/service/error-handler/app-error-handler';
import { User } from '../../schema/user';

@Injectable({
  providedIn: 'root',
})
export class UserService extends ErrorHandlerService {
  private url = `${environment.backendUrl}/api/users`;
  constructor(private http: HttpClient) {
    super();
  }
  getCurrentUserInfo(): Observable<User> {
    return this.http
      .get<User>(`${this.url}/current`)
      .pipe(catchError(this.handleError));
  }
  registerUser(register: RegisterModel): Observable<RegisterModel> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http
      .post<RegisterModel>(
        this.url + '/register',
        JSON.stringify(register),
        httpOptions
      )
      .pipe(catchError(this.handleError));
  }
}
