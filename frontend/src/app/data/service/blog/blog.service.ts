import { environment } from './../../../../environments/environment';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpResponse,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Blog } from '../../schema/blog';
import { catchError, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BlogDetail } from '../../schema/blog-detail';
import { ErrorHandlerService } from 'src/app/shared/service/error-handler/app-error-handler';
import { BlogCreate } from '../../schema/blog-create';

@Injectable({
  providedIn: 'root',
})
export class BlogService extends ErrorHandlerService {
  private url = `${environment.backendUrl}/api/blogs`;
  constructor(private http: HttpClient) {
    super();
  }

  getBlogs(pageNumber: number, pageSize: number): Observable<Blog[]> {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString());
    return this.http
      .get<Blog[]>(this.url, { params })
      .pipe(retry(3), catchError(this.handleError));
  }
  getBlogByTagName(
    tagName: string,
    pageNumber: number,
    pageSize: number
  ): Observable<Blog[]> {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString());
    return this.http
      .get<Blog[]>(`${this.url}/tags/${tagName}`, { params })
      .pipe(retry(3), catchError(this.handleError));
  }
  getBlogBySearchString(searchString: string): Observable<Blog[]> {
    const params = new HttpParams().set('searchString', searchString);
    return this.http
      .get<Blog[]>(`${this.url}/search`, { params })
      .pipe(retry(3), catchError(this.handleError));
  }
  getBlogById(id: string): Observable<BlogDetail> {
    return this.http
      .get<BlogDetail>(`${this.url}/${id}`)
      .pipe(retry(3), catchError(this.handleError));
  }

  createImageDisplay(blogId, imageDisplay): Observable<string> {
    const formData = new FormData();
    formData.append('imageDisplay', imageDisplay);
    return this.http
      .post<any>(`${this.url}/images/display-image/${blogId}`, formData)
      .pipe(catchError(this.handleError));
  }
  createBlog(blog: BlogCreate): Observable<BlogDetail> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http
      .post<BlogDetail>(this.url, blog, httpOptions)
      .pipe(catchError(this.handleError));
  }
  deleteImageBlog(folder): Observable<{}> {
    return this.http
      .delete<any>(`${this.url}/images/blog-image/${folder}`)
      .pipe(catchError(this.handleError));
  }
}
