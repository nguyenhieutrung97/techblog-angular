import { Tag } from './tag';

export class BlogCreate {
  constructor(title?: string, htmlContent?: string, tags?: Tag[]) {
    this.title = title;
    this.htmlContent = htmlContent;
    this.tags = tags;
  }
  title: string;
  htmlContent: string;
  tags: Tag[];
}
