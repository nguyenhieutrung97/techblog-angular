import { Tag } from './tag';

export class Blog {
  constructor(
    id?: string,
    title?: string,
    createdTime?: Date,
    imageUrl?: string,
    nameAuthor?: string,
    imageAuthorUrl?: string,
    tags?: Tag[]
  ) {
    this.id = id;
    this.title = title;
    this.createdTime = createdTime;
    this.imageUrl = imageUrl;
    this.nameAuthor = nameAuthor;
    this.imageAuthorUrl = imageAuthorUrl;
    this.tags = tags;
  }
  id: string;
  title: string;
  createdTime: Date;
  imageUrl: string;
  nameAuthor: string;
  imageAuthorUrl: string;
  tags: Tag[];
}
