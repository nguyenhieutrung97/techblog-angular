export class CommentCreate {
  constructor(blogId?: string, commentContent?: string) {
    this.blogId = blogId;
    this.commentContent = commentContent;
  }
  blogId: string;
  commentContent: string;
}
