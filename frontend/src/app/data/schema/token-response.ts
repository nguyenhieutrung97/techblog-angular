export class TokenResponse {
  nameDisplay: string;
  token: string;
  role: string;
}
