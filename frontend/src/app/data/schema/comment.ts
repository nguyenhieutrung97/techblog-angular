export class Comment {
  id: string;
  commentContent: string;
  createdTime: Date;
  userId: string;
  nameDisplayUser: string;
  imageUrlUser: string;
  blogId: string;
}
