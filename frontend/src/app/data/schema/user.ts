export class User {
  id: string;
  email: string;
  nameDisplay: string;
  imageUrl: string;
  createdTime: Date;
  blogCount: number;
  role: string;
  token: string;
}
