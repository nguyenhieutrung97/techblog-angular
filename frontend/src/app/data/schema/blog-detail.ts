import { Tag } from './tag';

export class BlogDetail {
  constructor() {
    this.blogId = '';
    this.title = '';
    this.imageUrl = '';
    this.htmlContent = '';
    this.createdTime = new Date();
    this.nameAuthor = '';
    this.emailAuthor = ';';
    this.imageAuthorUrl = '';
    this.tags = [];
  }
  blogId: string;
  title: string;
  imageUrl: string;
  htmlContent: string;
  createdTime: Date;
  nameAuthor: string;
  emailAuthor: string;
  imageAuthorUrl: string;
  tags: Tag[];
}
