import { ErrorModel } from './error-model';

export class BadRequestError extends ErrorModel {}
