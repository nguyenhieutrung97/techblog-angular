import { ErrorModel } from './error-model';

export class NotFoundError extends ErrorModel {}
