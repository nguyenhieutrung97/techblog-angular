import { AuthenticationService } from './../../../data/service/auth/authentication.service';
import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';
import { NotFoundError } from 'src/app/shared/service/error-handler/errror-handler-schema/notfound-error-model';
import { throwError } from 'rxjs';
import { UnauthorizedModel } from './errror-handler-schema/unauthorized-error-model';

export class ErrorHandlerService implements ErrorHandler {
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
      if (error.status === 400) {
        return throwError(new BadRequestError('Bad Request'));
      } else if (error.status === 404) {
        return throwError(new NotFoundError('Not Found'));
      } else if (error.status === 401) {
        localStorage.removeItem('tokenResponse');
        location.reload();
        return throwError(new UnauthorizedModel('Unauthorized'));
      }
    }
    console.log(error);
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
