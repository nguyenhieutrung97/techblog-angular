import { Directive, Input, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appInputFormat]',
})
export class InputFormatDirective {
  @Input('appInputFormat') format: string;
  standardSentence(input: string): string {
    input = input.trim();
    while (input.includes('  ')) {
      input = input.replace('  ', ' ');
    }
    return input;
  }
  titleCaseSentence(input): string {
    input = this.standardSentence(input);
    const sentence = input.toLowerCase().split(' ');
    for (let i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }
    return sentence.join(' ');
  }
  constructor(private el: ElementRef) {}
  @HostListener('blur') onBlur() {
    if (this.format === 'titlecase') {
      const value = this.el.nativeElement.value;
      if (value !== '') {
        this.el.nativeElement.value = this.titleCaseSentence(value);
      }
    }
  }
}
