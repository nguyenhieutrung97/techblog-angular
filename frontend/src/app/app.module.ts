import { AuthGuardService } from './shared/service/auth-guard/auth-guard.service';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { ContentBodyComponent } from './layout/content-body/content-body.component';
import { LoginModalComponent } from './layout/header/login-modal/login-modal.component';
import { RegisterModalComponent } from './layout/header/register-modal/register-modal.component';
import { HomeComponent } from './modules/home/home.component';
import { TagsNavbarComponent } from './modules/tags-navbar/tags-navbar.component';
import { BlogDetailComponent } from './modules/blog-detail/blog-detail.component';
import { AboutComponent } from './modules/about/about.component';
import { BlogItemComponent } from './modules/blog-item/blog-item.component';
import { BlogCreateComponent } from './modules/blog-create/blog-create.component';
import { NotFoundPageComponent } from './shared/component/not-found-page/not-found-page.component';
import { ServerErrorPageComponent } from './shared/component/server-error-page/server-error-page.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { JwtInterceptorService } from './shared/jwt-interceptor/jwt-interceptor.service';
import { UserInformationComponent } from './modules/user-information/user-information.component';
import { CommentSectionComponent } from './modules/comment-section/comment-section.component';
import { CommentItemComponent } from './modules/comment-section/comment-item/comment-item.component';
import { InputFormatDirective } from './shared/directive/input-format/input-format.directive';
import { MustMatchDirective } from './shared/directive/must-match/must-match.directive';
import { DompurifyPipe } from './shared/pipe/dompurity/dompurify.pipe';
import { AdminComponent } from './modules/admin/admin.component';
import { TagsManagerComponent } from './modules/admin/tags-manager/tags-manager.component';
import { TagCreateComponent } from './modules/admin/tags-manager/tag-create/tag-create.component';
import { TagDeleteComponent } from './modules/admin/tags-manager/tag-delete/tag-delete.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContentBodyComponent,
    LoginModalComponent,
    RegisterModalComponent,
    HomeComponent,
    TagsNavbarComponent,
    BlogDetailComponent,
    AboutComponent,
    BlogItemComponent,
    BlogCreateComponent,
    NotFoundPageComponent,
    ServerErrorPageComponent,
    UserInformationComponent,
    CommentSectionComponent,
    CommentItemComponent,
    InputFormatDirective,
    MustMatchDirective,
    DompurifyPipe,
    AdminComponent,
    TagsManagerComponent,
    TagCreateComponent,
    TagDeleteComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgSelectModule,
    CKEditorModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'search', component: HomeComponent },
      { path: 'about', component: AboutComponent },
      {
        path: 'admin',
        component: AdminComponent,
        children: [
          {
            path: 'tag-manager',
            component: TagsManagerComponent, // child route component that the router renders
          },
        ],
        canActivate: [AuthGuardService],
      },
      {
        path: 'user',
        component: UserInformationComponent,
        canActivate: [AuthGuardService],
      },
      { path: 'severerror', component: ServerErrorPageComponent },
      {
        path: 'blogs/create',
        component: BlogCreateComponent,
        canActivate: [AuthGuardService],
      },
      { path: 'blogs/:id', component: BlogDetailComponent },
      { path: ':tagName', component: HomeComponent },
      { path: '**', component: NotFoundPageComponent },
    ]),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
