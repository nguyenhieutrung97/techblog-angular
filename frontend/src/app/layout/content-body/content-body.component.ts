import { UserService } from 'src/app/data/service/user/user.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/data/service/auth/authentication.service';
import { User } from 'src/app/data/schema/user';

@Component({
  selector: 'app-content-body',
  templateUrl: './content-body.component.html',
  styleUrls: ['./content-body.component.css'],
})
export class ContentBodyComponent implements OnInit {
  urlHost = environment.backendUrl;
  currentUser: User = null;
  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe((response) => {
      if (response !== null) {
        this.userService.getCurrentUserInfo().subscribe((user) => {
          this.currentUser = { ...user };
        });
      } else {
        this.currentUser = null;
      }
    });
  }
}
