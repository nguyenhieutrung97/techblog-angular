import { Component, OnInit, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/data/service/user/user.service';
import { ErrorModel } from 'src/app/shared/service/error-handler/errror-handler-schema/error-model';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';

@Component({
  selector: 'register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css'],
})
export class RegisterModalComponent implements OnInit {
  constructor(
    private el: ElementRef,
    private userService: UserService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}
  regiser(form) {
    const registerModel = { ...form.value };
    delete registerModel.confirmPassword;
    this.userService.registerUser(registerModel).subscribe(
      (response) => {
        console.log(response);
        form.reset();
        this.el.nativeElement.querySelector('#close-register-modal').click();
        this.toastr.success(
          `User ${response.nameDisplay} register successful!!!`
        );
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.warning(
            'Email already existed!!! Please choose another email!!!'
          );
        } else {
          this.toastr.error(error);
        }
      }
    );
  }
  onSubmit(form) {
    if (form.valid) {
      this.regiser(form);
    } else {
      const firstInvalidControl = this.el.nativeElement.querySelector(
        '#register-modal input.ng-invalid'
      );
      if (firstInvalidControl) {
        firstInvalidControl.focus();
      }
    }
  }
}
