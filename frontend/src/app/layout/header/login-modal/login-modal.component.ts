import { AuthenticationService } from './../../../data/service/auth/authentication.service';
import { UserService } from 'src/app/data/service/user/user.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BadRequestError } from 'src/app/shared/service/error-handler/errror-handler-schema/badrequest-error-model';
import { User } from 'src/app/data/schema/user';

@Component({
  selector: 'login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css'],
})
export class LoginModalComponent implements OnInit {
  constructor(
    private el: ElementRef,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}
  onChange(x) {
    console.log(x);
  }
  login(form) {
    this.authenticationService.login(form.value).subscribe(
      (response: User) => {
        form.reset();
        this.el.nativeElement.querySelector('#close-login-modal').click();
        this.toastr.success(`User ${response.nameDisplay} login successful!!!`);
      },
      (error) => {
        if (error instanceof BadRequestError) {
          this.toastr.warning('Account not invalid');
        } else {
          this.toastr.error(error);
        }
      }
    );
  }
  onSubmit(form) {
    if (form.valid) {
      this.login(form);
    } else {
      const firstInvalidControl = this.el.nativeElement.querySelector(
        '#login-modal input.ng-invalid'
      );
      if (firstInvalidControl) {
        firstInvalidControl.focus();
      }
    }
  }
}
