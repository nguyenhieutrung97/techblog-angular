import { Router } from '@angular/router';
import { TokenResponse } from './../../data/schema/token-response';
import { AuthenticationService } from './../../data/service/auth/authentication.service';
import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/data/service/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  tokenResponse: TokenResponse;
  searchString: string = '';
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe((response) => {
      this.tokenResponse = response;
    });
  }
  onClickSearch() {
    this.router.navigate(['/search'], {
      queryParams: { searchString: this.searchString },
    });
  }
  onClickLogout() {
    this.authenticationService.logout();
    this.router.navigate(['/']);
  }
}
