import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  linkItems = [
    { link: '/', title: 'Home' },
    { link: '/about', title: 'About' }
  ];
  constructor() {}

  ngOnInit(): void {}
}
